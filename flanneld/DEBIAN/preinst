#!/bin/bash -e
#
# This script is executed in the pre-installation phase
#
#   On Debian,
#       $1=install : indicates an new install
#       $1=upgrade : indicates an upgrade
#
#   On RedHat,
#       $1=1       : indicates an new install
#       $1=2       : indicates an upgrade

case "$1" in

    # Debian ####################################################
    install|upgrade)

        # Create kubernetes group if not existing
        if ! getent group kubernetes > /dev/null 2>&1 ; then
            echo -n "Creating kubernetes group..."
            addgroup --quiet --system kubernetes
            echo " OK"
        fi

        # Create flannel user if not existing
        if ! id flannel > /dev/null 2>&1 ; then
            echo -n "Creating flannel user..."
            adduser --quiet \
                    --system \
                    --home /var/lib/flannel \
                    --ingroup kubernetes \
                    --disabled-password \
                    --shell /bin/false \
                    flannel
            echo " OK"
        fi
    ;;
    abort-deconfigure|abort-upgrade|abort-remove)
    ;;

    # RedHat ####################################################
    1|2)

        # Create kubernetes group if not existing
        if ! getent group kubernetes > /dev/null 2>&1 ; then
            echo -n "Creating kubernetes group..."
            groupadd -r kubernetes
            echo " OK"
        fi

        # Create flannel user if not existing
        if ! id flannel > /dev/null 2>&1 ; then
            echo -n "Creating flannel user..."
            useradd -r \
                    -M \
                    --gid flannel \
                    --shell /sbin/nologin \
                    --comment "flannel user" \
                    flannel
            echo " OK"
        fi
    ;;

    *)
        echo "pre install script called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

exit 0
